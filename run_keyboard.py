#!/usr/bin/python3
import datetime
import time

from uicon import *
toggle = 0
vt_cursor_off()
vt_clear()

#TODO pass in when started
compositerm_size = [90,90]
#      rows, cols
size = [39,90]
click_map = {}
start_app_text = "   run"

keyboard_text_small = """
1 2 3 4 5 6 7 8 9 0 - = back
q w e r t y u i o p [ ] \\
a s d f g h j k l ; ' enter
z x c v b n m , . /
tab shift space ctrl super alt"""
keyboard_data = []
for line in keyboard_text_small.split("\n"):
    if line == "":
        continue
    keyboard_data.append(line.split(" "))

script_path = os.path.dirname(os.path.realpath(__file__))


def open_app(row, col, row_len, col_len, cmd):
    with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"{row},{col}:{col_len},{row_len}:{cmd}\n")

def close_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"close:{index}")

def min_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"min:{index}")
        
def max_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"max:{index}")

def send_key(key):
    with open("/dev/shm/compositerm/vkb", 'a') as fh:
        if key == "enter":
            key = "\n"
        fh.write(f"{key}\n")
    print(key)

def get_running(simple=False):
    stuff_running = {}
    with open("/dev/shm/compositerm/running") as fh:
        for running_app in fh.readlines():
            index,cmd = running_app.split(":")
            if simple:
                if "/" in cmd:
                    simple_cmd = cmd.split("/")[-1].strip()
                else:
                    simple_cmd = cmd
                stuff_running[simple_cmd] = index
            else:
                stuff_running[index] = cmd
    return stuff_running

def process_click(row,col):
    global click_map
    global compositerm_size
    col_len = size[1]
    row_len = size[0]
    
    for button_name in click_map:
        button_data = click_map[button_name]
        start_row, start_col = button_data[0]
        num_rows, num_cols =  button_data[1]
        if col >= start_col:
            if col < start_col + num_cols:
                #print(f"Withing col {button_name}")
                if  row >= start_row:
                    if row < start_row + num_rows:
                        send_key(button_name)
                        #WE have a click :)
                        #print(button_name)
                        #print(click_map)
                        #print((row, col))
                        return()


#This also updates the click_map
def print_keyboard():
    global click_map
    global start_app_text
    global keyboard_data
    
    keyboard_vt = ""
    col_len = size[1]
    row_len = size[0]
    
    button_row_len = int(row_len/len(keyboard_data))
    button_col_len = int(col_len/len(keyboard_data[0])) #TODO might crash if the top row is not the longest
    
    #apps_running = dict(get_running(simple=True))
    #num_things_to_write = len(apps_running) + 1
    #size_each_gets = int(col_len)/int(num_things_to_write)
    for keyboard_row_index in range(0, len(keyboard_data)):
        for keyboard_key_index in range(0, len(keyboard_data[keyboard_row_index])):
            char_or_name = keyboard_data[keyboard_row_index][keyboard_key_index]
            row = keyboard_row_index * button_row_len
            col = keyboard_key_index * button_col_len
            #Setup click map
            #                         [[row,col],[num_rows      ,num_cols      ]]
            click_map[char_or_name] = [[row,col],[button_row_len,button_col_len]]
            
            #Setup vt
            if len(char_or_name) >= button_col_len:
                keyboard_vt = keyboard_vt + char_or_name[:button_col_len]
            else:
                extra_space_len = int(button_col_len - len(char_or_name))
                extra_spaces = " " * extra_space_len
                keyboard_vt = keyboard_vt + char_or_name + extra_spaces
        keyboard_vt = keyboard_vt + "\n" * button_row_len
    
    #return(str(click_map))
    return(keyboard_vt)
#clear does not work well with background colors within compositerm. This is a bad workaround. :) 
#def clear_by_dump():
#    vt_move(0,0)
#    x, y = vt_size()
#    spaces = " " * (x * y)
#    vt_write(spaces)

for event in uicon():
    if "init" in event[0]:
        vt_clear()
        vt_move(0,0)
        vt_write(print_keyboard())
    if "left_mouse_up" in event[0]:
        col,row = event[0].split(" ")[-2:]
        process_click(int(row),int(col))
        
        #redraw
        time.sleep(0.2)
        vt_clear()
        vt_move(0,0)
        vt_write(print_keyboard())
        #vt_move(y,x)
        #vt_write('o')
        #print(x,y)
        
    #print(f"DRIVER: {event}")
