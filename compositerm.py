#!/usr/bin/python3
import time
import os
import subprocess
import threading
import copy

#import fcntl
#import struct
import tempfile
import mmap
import pyte
import pty
from basic_vt import *

#TODO check if this is running for auto set
#False = use touch driver
USE_GPM = False
#INPUT_DEVICE = "/dev/input/event6"
L5_input = "cat /proc/bus/input/devices | grep -P '^[NH]: ' | paste - - | grep EP0700M09 | cut -d '='  -f3 | cut -d ' ' -f1"
INPUT_DEVICE=f"/dev/input/{os.popen(L5_input).read()}".strip()

terminal_size = vt_size()
script_path = os.path.dirname(os.path.realpath(__file__))


#mouse stuff
mouseEvent = []
clickTime = time.time()
RUNNING = []
MINIMIZED = []
force_while_loop = False
keyboard_blacklist = [f"{script_path}/run_keyboard.py"]
#working_dir=f"/dev/shm/{time.time()}/"
working_dir=f"/dev/shm/compositerm/"
try:
    os.mkdir(working_dir)
except Exception:
    pass

#Used to open ttys
#x,y:cols,row:script_path
composit_hook = f"{working_dir}hook"
try:
    os.mknod(composit_hook)
except Exception:
    pass

#Used to show info about started ttys
composit_running = f"{working_dir}running"

try:
    os.mknod(composit_running)
except Exception:
    with open(composit_running, "w") as fh:
        fh.write("")

#Used to send keys to the compositor
vkb = f"{working_dir}vkb"

try:
    os.mknod(vkb)
except Exception:
    with open(vkb, "w") as fh:
        fh.write("")

mouse_pipe_name=f"/dev/shm/gpm_pipe"
#os.mknod(mouse_pipe)
#print(mouse_pipe)

#example_screen_obj = {'position': [0,0], 'size': [90,66], 'cmd': '/bin/bash'}
needs_started = []
next_tty_index = 0
process_list = []
masters = []
slaves = []
slave_fd, tmpfile = tempfile.mkstemp()
os.write(slave_fd, b'\x00' * mmap.PAGESIZE)
os.lseek(slave_fd, 0, os.SEEK_SET)

output_data = []
history_buffer = []
rendered_data = []
streams = []
bash_threads = []
screens = []
dirty_screens = []
tty_positions = {}
tty_sizes = {}
last_rendered_mouse = [0,0]
mouse_position = [0,0]
clicks = []
space_to_clear = []
target_tty = 0

#clear screen and get going. :)
vt_clear()
#Only used by Draw inside the draw loop/thread
def write_char(char_obj, display_pos):
    global the_displayed
    global terminal_size
    
    
    
    #init process list if needed
    if 'the_displayed' not in list(globals().keys()):
        (rows, cols) = terminal_size
        the_displayed  = [ [{}]*cols for i in range(rows)]
    #debug((display_pos[0], display_pos[1]))
    old_char_obj = the_displayed[display_pos[0]][display_pos[1]]
    if char_obj == "clear":
        vt_write(color_as_vt("white","black",False))
        vt_move(display_pos[0],display_pos[1])
        vt_write(" ")
        return()
    if char_obj == None:
        if old_char_obj == {}:
            char = " "
        else:
            char = old_char_obj.data
        vt_move(display_pos[0],display_pos[1])
        vt_write(char)
        return()
    if char_obj != old_char_obj:
        if isinstance(char_obj, str):
            char = char_obj
            if old_char_obj != {}:
                bg_color = old_char_obj.bg
                fg_color = old_char_obj.fg
                bold = old_char_obj.bold
            else:
                bg_color = "black"
                fg_color = "white"
                bold = False
            vt_write(color_as_vt(fg_color,bg_color,bold))
            vt_move(display_pos[0],display_pos[1])
            vt_write(char)
        else:
            char = char_obj.data
            #debug(f"char '{char}'")
            bg_color = char_obj.bg
            fg_color = char_obj.fg
            bold = char_obj.bold
            vt_move(display_pos[0],display_pos[1])
            vt_write(color_as_vt(fg_color,bg_color,bold))
            vt_write(char)


def process_click(row,col):
    global tty_positions
    global tty_sizes
    global next_tty_index
    global target_tty #used for keyboard input
    global keyboard_blacklist
    
    
    debug(f"row{row}:col{col}")
    for tty_index in range(0,next_tty_index):
        try:
            data = tty_positions[tty_index]
            data2 = tty_sizes[tty_index]
        except KeyError:
            debug(f"TTY might be closed: {tty_index}")
            continue
        test_tty_start_row = data[0]
        test_tty_start_col = data[1]
        test_tty_size_x = data2[1]
        test_tty_size_y = data2[0]
        test_tty_end_row = test_tty_start_row + test_tty_size_x
        test_tty_end_col = test_tty_start_col + test_tty_size_y
        
        debug(f"start_col {test_tty_start_col} row {col} max {test_tty_size_y}")
        debug(f"Checking {data}")
        test_x = row - test_tty_start_row + 1
        test_y = col - test_tty_start_col
        if test_x > 0 and test_y > 0:
            if test_x <= test_tty_size_x and test_y < test_tty_size_y:
                debug(f"sending click: {test_x} {test_y}")
                #(color_as_vt("green","red",False), tty_index)
                #bash_run(vt_click_down(test_x, test_y), tty_index)
                bash_run(vt_click_up(test_x, test_y), tty_index)
                #TODO check if this is a blacklisted program
                running_things = read_running(flat=True)
                debug(f"running_things {running_things}")
                if tty_index in running_things:
                    program = running_things[tty_index]
                    if program not in keyboard_blacklist:
                        debug(f"Set target to: {program}")
                        
                        target_tty = tty_index
        

def draw():
    global mouse_position
    global dirty_screens
    global output_data
    global tty_positions
    global tty_sizes
    global last_rendered_mouse
    global clicks
    global space_to_clear
    #really bad, but fine for now
    #if screen_dirty:
    #    vt_clear()
    #    screen_dirty = False
    #vt_move(int(mouse_position[1]) - 1, int(mouse_position[0]) - 1)
    #vt_write("+" + str(mouse_position))
    while True:
        time.sleep(0.2)
        
        if clicks != []:
            for click_xy in clicks:
                clicks.remove(click_xy)
                process_click(click_xy[1],click_xy[0])
        
        if last_rendered_mouse != mouse_position:
            #debug(f"MOUSE, {mouse_position}")
            write_char(None, last_rendered_mouse)
            
            write_char('x', mouse_position)
            last_rendered_mouse = mouse_position
        #Draw ttys
        for dirty_tty in dirty_screens:
            dirty_screens.remove(dirty_tty)
            debug(f"dirty_tty {dirty_tty}")
            if output_data[dirty_tty] != "":
                debug(f"drawing screen: {dirty_tty}")
                #This data might change, copy it first
                try:
                    raw_data = copy.deepcopy(output_data[dirty_tty])
                except RuntimeError:
                    debug("This is not very thread safe... Hmmm")
                    #setup callback
                    time.sleep(0.2)
                    dirty_screens.append(dirty_tty)
                    draw()
                for line_index in raw_data:
                    line = raw_data[line_index]
                    for col_index in line:
                        char_obj = line[col_index]
                        char_pos = [line_index, col_index]
                        #might be removed if this tty was just closed
                        try:
                            offset = tty_positions[dirty_tty]
                        except KeyError:
                            #debug(f"offset not available for index {dirty_tty}")
                            continue
                        display_pos = [char_pos[0]+offset[0], char_pos[1]+offset[1]]
                        write_char(char_obj, display_pos)                        
                        #debug(f"Yo: {char_obj.data} {display_pos}")
        
        #clear closed ttys
        for needs_cleared in space_to_clear:
            space_to_clear.remove(needs_cleared)
            tty_start_row, tty_start_col, tty_size_x, tty_size_y = needs_cleared
            for row in range(tty_start_row, tty_start_row + tty_size_x):
                for col in range(tty_start_col, tty_start_col + tty_size_y):
                    write_char("clear", [row,col])
        

def click(x, y, right=False):
    #This is a placeholder for terminals running in X11/Wayland (Not supported) TODO
    debug("YoYO")

#Setup term
vt_enable_mouse()
os.system("stty -icanon -echo")

def get_slaves():
    raw = get_slaves_raw()
    debug(raw)
    return_data = []
    for less_raw in raw.strip('/').split('/'):
        return_data.append("/dev/pts/" + str(less_raw))
    return return_data


def get_slaves_raw():
    os.lseek(slave_fd, 0, os.SEEK_SET)
    buf = mmap.mmap(slave_fd, mmap.PAGESIZE, mmap.MAP_SHARED, mmap.PROT_READ)
    msg = str(buf.readline())
    msg = ':'.join(msg.split(':')[:-1]).split("'")[-1]
    return(msg)


def add_slave(pid):
    global slaves
    offset = get_slaves_raw()
    offset = len(offset)
    os.lseek(slave_fd, offset, os.SEEK_SET)
    buf = mmap.mmap(slave_fd, mmap.PAGESIZE, mmap.MAP_SHARED, mmap.PROT_WRITE)
    TTY = subprocess.check_output(['ps', 'hotty', str(pid)]).strip().decode()
    TTY = TTY.split("pts")[-1] + ":" # cut off pts and add a : for spacing
    #slaves.append(TTY)
    for index in range(offset, offset + len(TTY)):
        buf[index] = ord(TTY[index - offset])

def read_running(flat=False):
    global composit_running
    
    if flat:
        nice_looking_list_of_stuff_running = {}
    else:
        nice_looking_list_of_stuff_running = []
    with open(composit_running) as tmp_file_handle:
        for line in tmp_file_handle.readlines():
            if line != "":
                line = line.strip()
                index,cmd = line.split(":")
                if flat:
                    nice_looking_list_of_stuff_running[int(index)] = cmd
                else:
                    nice_looking_list_of_stuff_running.append({index:cmd})
    return(nice_looking_list_of_stuff_running)
    

def add_cli(index,cmd):
    global composit_running
    
    if index not in read_running():
        with open(composit_running, 'a') as tmp_file_handle:
            index_and_whatnot = f"{index}:{cmd}\n"
            tmp_file_handle.write(index_and_whatnot)
    else:
        debug(f"Error adding duplicate index {index}:{cmd}")


def remove_cli(index):
    stuff_running = read_running()    
    #TODO
    #clear composit_running
    open(composit_running, 'w').close()
    
    #loop stuff_running and add it back
    for still_open in stuff_running:
        open_index = list(still_open.keys())[0]
        if int(open_index) != int(index):
            debug(f"still running {still_open}")
            cmd = still_open[open_index]
            add_cli(open_index, cmd)
    
        

def bash_screen_ref(cmd, local_screen):
    global streams
    global screens
    global output_data
    global dirty_screens
    global composit_running
    global RUNNING
    
    #store current output screen
    debug("OPENING SCREEN WITH DISPLAY: " + str(local_screen))
    
    #store what we just ran
    add_cli(local_screen, cmd)
    composit_running
    output_index = 0
    for output_bit in bash_attach(cmd, local_screen):

        if local_screen not in RUNNING:
            remove_cli(local_screen)
            debug("end bash")
            #exit()
        #debug("output_bit: " + str(output_bit))

        #update main terminal (Debug only)
        #VT = output_bit.decode('utf-8', errors='ignore')
        #vt_write(VT)
        #feed vt100 into pyte
        streams[local_screen].feed(output_bit)
        
        #don't update output if minimized
        if local_screen in MINIMIZED:
            continue
        #read screen data
        tmp_data = screens[local_screen].buffer

        if tmp_data != "":
            debug(f"Stuff from {local_screen}")
            output_data[local_screen] = tmp_data
            dirty_screens.append(local_screen)
        
        

def open_ttys():
    global output_data #   only used to init new bash session
    global history_buffer# needs to be the size of the open screens
    global rendered_data #init as well...
    global masters #bash sessions input
    global streams
    global force_while_loop #most likely not needed
    global bash_threads
    global next_tty_index #used to ctrl which screen is active
    global screens #pyte screens
    global out_put_screen #tty index of screen we are opening
    global needs_started
    global tty_positions
    global tty_positions
    global RUNNING

    if needs_started != []:
        for needed_tty in needs_started:
            #needed_tty should look like: 
            ##{'position': [0,0], 'size': [90,66], 'cmd': '/bin/bash'}
            term_size = needed_tty['size']
            cmd = needed_tty['cmd']
            #run bash while piping it's ouput/err/in to new pts
            
            #setup new screen index
            out_put_screen = next_tty_index
            next_tty_index = next_tty_index + 1
            
            RUNNING.append(out_put_screen)
            
            
            tty_positions[out_put_screen] = needed_tty['position']
            tty_sizes[out_put_screen] = needed_tty['size']
            
            #init new bash session
            debug("NEW bash session")
            output_data.append([])
            rendered_data.append([])
            history_buffer.append([])

            #kick off shell thread (the middle bash part)
            #master_new, slave_new = pty.openpty()
            #masters.append(master_new)
            #masters.append(slave_new)
            #open new virt term
            #new_screen = pyte.Screen(term_size[1], term_size[0] - 2)
            debug("Opening tty with size: " + str(term_size))
            new_screen = pyte.screens.HistoryScreen(term_size[0],term_size[1],history=1000)
            #debug("80 : " + str(term_size[1]))
            screens.append(new_screen)
            streams.append(pyte.ByteStream(screens[-1]))

            streams[-1].escape["N"] = "next_page"
            streams[-1].escape["P"] = "prev_page"



            #win_size = struct.pack("HHHH", term_size[0] - 2, term_size[1], 0, 0)
            #fcntl.ioctl(masters[-1], termios.TIOCSWINSZ, win_size)

            #debug(dir(streams.copy), Level=3)
            #wait for tty to be ready
            #try to a connect over socket
            bash_threads.append(threading.Thread(target=bash_screen_ref, args=(cmd,out_put_screen)))
            bash_threads[-1].start()
            #vt_clear()
            #vt_move(0,0)
            
            #resize tty:
            #screens[-1].resize(lines=term_size[0], columns=term_size[1])
            #screens[-1].set_margins(term_size[0], term_size[1])
            #vt_send(resize_as_vt(term_size[0], term_size[1]), out_put_screen)
            #vt_send("\n\n\n", out_put_screen)
            
            #bash_run('echo "$(tput cols)x$(tput lines)";tty\n', out_put_screen)
            #bash_run('./top_bar.py\n', out_put_screen)
            #c = 'KEY_RESIZE' #force resize
            #force_while_loop = True
            #wait for term to move
            time.sleep(0.15)
        #everything should be running now
        #TODO remove these as we process them, in case something else is added while we are processing stuff. 
        needs_started = []

def bash_run(cmd, index, redo=True):
    global masters
    try: #If tty are opened fast, this can crash
        os.write(masters[index], cmd.encode())
    except Exception:
        time.sleep(.2) #wait for tty
        if redo:
            bash_run(cmd, index, redo=False)

def vt_send(vt_data, index, redo=True):
    global slaves
    try: #If tty are opened fast, this can crash
        os.write(slaves[index], vt_data.encode())
    except Exception:
        time.sleep(.2) #wait for tty
        if redo:
            vt_send(vt_data, index, redo=False)


def bash_attach(cmd, screen_id):
    global needs_started
    global process_list
    global masters

    #init process list if needed
    if 'process_list' not in list(globals().keys()):
        process_list = []


    #Thanks! https://stackoverflow.com/a/52157066/5282272
    # fork this script such that a child process writes to a pty that is
    # controlled or "spied on" by the parent process

    (child_pid, new_master_handle) = pty.fork()
    masters.append(new_master_handle)
    # A new child process has been spawned and is continuing from here.
    # The original parent process is also continuing from here.
    # They have "forked".

    if child_pid == 0:
        debug("This is the child process fork, pid %s" % os.getpid())
        debug("ADDING")
        add_slave(os.getpid())
        debug(f"shared map: {get_slaves_raw()}")
        debug(get_slaves())
        process_list.append(subprocess.run(cmd))

    else:
        debug("This is the parent process fork, pid %s" % os.getpid())

        while True:
            try:
                data = os.read(masters[screen_id], 1026)
            except Exception:
                #time.sleep(.2)
                continue
            yield data

def debug(error_string):
    global DEBUG
    global DEBUG_LOG
    DEBUG_LOG = "/dev/shm/compositerm/debug"
    
    if DEBUG:
        with open(DEBUG_LOG, 'a+') as the_log:
            the_log.write(str(error_string) + "\n")
        #print(error_string, file=sys.stderr)



def composit_process(event):
    global mouse_position
    global target_tty
    global clicks
    global masters
    #setup any ttys
    #open_ttys()
    #handle mouse events
    if len(event) == 3:
        debug(f"mouse_event {event[0]}")
        if event[0] in ["drag", "click", "move"]:
            mouse_position = list(reversed(event[1:]))
            
        #TODO send click event into tty (like xterm)
        if event[0] == "click":
            clicks.append(event[-2:])
            pass
        
        #don't clear screen for drag
        if event[0] != "drag":
            pass

    #handle keyboard events
    elif len(event) == 2:
        key = event[1]
        #debug(f"Sending {key} to TTY index {target_tty}")
        if type(key) == bytes:
            os.write(masters[target_tty], key)
        elif type(key) == str:
            os.write(masters[target_tty], key.encode())
        #debug reset mouse
        #vt_move(0,0)
    

#thanks https://medium.com/@aliasav/how-follow-a-file-in-python-tail-f-in-python-bca026a901cf
def follow(thefile):
    '''generator function that yields new lines in a file
    '''
    # seek the end of the file
    thefile.seek(0, os.SEEK_END)
    
    # start infinite loop
    while True:
        # read last line of file
        line = thefile.readline()        # sleep if file hasn't been updated
        if not line:
            time.sleep(0.1)
            continue

        yield line


###################################################
################trigger/virt keyboard##############
###################################################

def manage_virt_board():
    vkb_hook = open(vkb)
    #TODO clear after read
    for key in follow(vkb_hook):
        debug(f"Virtboard: {key}")
        key = key.strip("\n")
        if key == "":
            key = "\n"
        composit_process((key, key))

virt_thread = threading.Thread(target=manage_virt_board)
virt_thread.daemon = True
virt_thread.start()
###################################################
################trigger/hook stuff#################
###################################################
#echo "0,0:90,44:/bin/bash" >> /dev/shm/compositerm/hook
#This will open a tty running at x=0, y=0 size 90x44 running bash

#echo "45,0:90,44:/bin/bash" >> /dev/shm/compositerm/hook

#echo "close:1" >> /dev/shm/compositerm/hook
#will close the tty at index 1

#echo "min:1" >> /dev/shm/compositerm/hook
#will minimize the tty at index 1
def open_when_asked_for():
    global needs_started
    global composit_hook
    global RUNNING
    global tty_positions
    global tty_sizes
    global space_to_clear
    global MINIMIZED
    global dirty_screens
    
    debug(composit_hook)
    composit_hook = open(composit_hook)
    for command in follow(composit_hook):
        debug(f"HOOK CMD: {command}")
        
        if command.startswith("min"):
            index = int(command.split(":")[-1].strip())
            if index not in MINIMIZED:
                MINIMIZED.append(index)
                #clear space:
                data = tty_positions[index]
                data2 = tty_sizes[index]
                tty_start_row = data[0]
                tty_start_col = data[1]
                tty_size_x = data2[1]
                tty_size_y = data2[0]

                space_to_clear.append([tty_start_row, tty_start_col, tty_size_x, tty_size_y])
                
            continue
        elif command.startswith("max"):
            index = int(command.split(":")[-1].strip())
            if index in MINIMIZED:
                MINIMIZED.remove(index)
                dirty_screens.append(index)
                
        if "close" in command:
            index = int(command.split(":")[-1].strip())
            if index in RUNNING:
                RUNNING.remove(index)
                #force some event
                bash_run(vt_click_up(-1, -1), int(index))

                data = tty_positions[index]
                tty_positions.pop(index)
                
                data2 = tty_sizes[index]
                tty_sizes.pop(index)
                
                tty_start_row = data[0]
                tty_start_col = data[1]
                tty_size_x = data2[1]
                tty_size_y = data2[0]

                space_to_clear.append([tty_start_row, tty_start_col, tty_size_x, tty_size_y])
                debug(f"Setup index {index} for removal")
            else:
                debug(f"Index {index} is not running. \nRunning: {RUNNING}")
            continue
        
        if ":" in command:
            command = command.strip()
            command = command.split(":")
        else:
            debug(f"Bad CMD: {command}")
            continue
        if len(command) == 3:
            cmd_pos  = command[0].split(',')
            cmd_size = command[1].split(',')
            cmd_path = command[2].strip()
            new_command = {}
            new_command['position'] = [ int(cmd_pos[0]),  int(cmd_pos[1])]
            new_command['size']     = [ int(cmd_size[0]), int(cmd_size[1])]
            new_command['cmd']     = cmd_path
            needs_started = [new_command]
        else:
            debug(f"Bad CMD: {command}")
            continue
        #new_command = {'position': [0,0], 'size': [90,66], 'cmd': '/bin/bash'}
        #needs_started = [new_command]
        open_ttys()
        
        
        #needs_started.append(command)
        #open_ttys()

hook_thread = threading.Thread(target=open_when_asked_for)
hook_thread.daemon = True
hook_thread.start()

####################################################
##################Display Thread####################
####################################################
draw_thread = threading.Thread(target=draw)
draw_thread.daemon = True
draw_thread.start()
####################################################
###############Librem 5/Touchscreen#################
####################################################
display_size = (720,1440)
def screen_to_pos(x,y):
    global size
    global terminal_size
    
    (term_rows, term_cols) = terminal_size
    term_rows = term_rows - 1
    term_cols = term_cols - 1
    x = int(x)
    y = int(y)
    if x == 0:
        new_x = x
    else:
        tmp = display_size[0]/x
        new_x = terminal_size[1]/tmp
    
    if y == 0:
        new_y = y
    else:
        tmp =  display_size[1]/y
        new_y = terminal_size[0]/tmp
    rounded_x = round(new_x)
    rounded_y = round(new_y)
    
    if rounded_x > term_cols:
        rounded_x = term_cols
    if rounded_y > term_rows:
        rounded_y = term_rows
        
    return(rounded_x, rounded_y)

def touch_driver():
    touch_cmd = f"sudo evtest {INPUT_DEVICE}"
    process = subprocess.Popen(touch_cmd, stdout=subprocess.PIPE, shell=True)
    #debug(touch_cmd)
    x = 0
    y = 0
    touching = -1
    while True:
        #debug("Touch test")
        output = process.stdout.readline()
        if output == '' and process.poll() is not None:
            break
        if output:
            output = str(output)
            if "BTN_TOUCH" in output:
                touching = output.split("value ")[-1]
                touching = touching.split("\\")[0]
                #debug (f"touching: {touching}")
                #check if we just tapped
                if touching == '0':
                    #debug(["click", x, y])
                    termx, termy = screen_to_pos(x,y)
                    composit_process(["click", termx, termy])
            if "ABS_X" in output:
                temp_x = output.split("value ")[-1]
                temp_x = temp_x.split("\\")[0]
                if temp_x.isdigit():
                    x = temp_x
                #TODO update x
                if touching == '1':
                    debug (["drag", x, y])
                    termx, termy = screen_to_pos(x,y)
                    composit_process(["drag", termx, termy])
            if "ABS_Y" in output:
                temp_y = output.split("value ")[-1]
                temp_y = temp_y.split("\\")[0]
                if temp_y.isdigit():
                    y = temp_y
                if touching == '1':
                    #debug (["drag", x, y])
                    termx, termy = screen_to_pos(x,y)
                    composit_process(["drag", termx, termy])
            #else:
                #print (f"stuff: {output}")
            #("move",x,y)
            
    rc = process.poll()

if not USE_GPM:
    debug("Starting touch driver")
    touch_thread = threading.Thread(target=touch_driver)
    touch_thread.daemon = True
    touch_thread.start()
    
####################################################
###############GPM driver (Desktop)#################
####################################################
def start_gpm_reader():
    os.system("gpm_reader")
    
def mouse_pipe():
    fh = open(mouse_pipe_name)
    for line in follow(fh):
        line = line.strip()
        (code,x,y) = line.split(":")
        debug(f"raw line: {(code,x,y)}")
        #mouse_up
        if code == '24':
            yield ("click",x,y)
        #move
        if code == '146' or code == '1':
            yield("move",x,y)
if USE_GPM:
    thread = threading.Thread(target=start_gpm_reader)
    thread.daemon = True
    thread.start()


    for event in mouse_pipe():
        composit_process(event)


####################################################
###############Process keyboard data################
####################################################
debug("STARTING KEYBOARD STUFFF")
#Process keyboard input
while True:
    try:
        debug('tick')
        repaint_needed = False

        # get keyboard input, returns -1 if none available (force_while_loop set to true will let you set c manually)
        if not force_while_loop:
            try:
                raw_c = sys.stdin.buffer.peek()
                c = sys.stdin.read(1)
                debug("read")
            except Exception as e:
                debug("Error reading from term")
                debug(e)
                time.sleep(0.2)
                #try again
                continue
        else:
            force_while_loop = False
            raw_c = ""

        ####################remap keys... ################
        #Mostly cuz I had to move away from curses (Cannot get keys on a diff thread than the one that draws)


        if len(c) < len(raw_c):
            #clear read buffer
            sys.stdin.read(len(raw_c) - len(c))

            #look for shit
            #          Ctrl + Right
            if raw_c == b'\x1b[1;5C':
                c = "kRIT5"

            #            Ctrl + Left
            elif raw_c == b'\x1b[1;5D':
                c = "kLFT5"

            #            Ctrl + up
            elif raw_c == b'\x1b[1;5A':
                c = "kUP5"

            #            Ctrl + Down
            elif raw_c == b'\x1b[1;5B':
                c = "kDN5"

            #            right
            elif raw_c == b'\x1b[C':
                c = "KEY_RIGHT"

            #            left
            elif raw_c == b'\x1b[D':
                c = "KEY_LEFT"

            #            up
            elif raw_c == b'\x1b[A':
                c = "KEY_UP"

            #            down
            elif raw_c == b'\x1b[B':
                c = "KEY_DOWN"

            #            Delete
            elif raw_c == b'\x1b[3~':
                c = "KEY_DC"

            #            Shift + Tab
            elif raw_c == b'\x1b[Z':
                c = "KEY_BTAB"

            #            Alt + .
            elif raw_c == b'\x1b.':
                c = "alt_dot"

            #            Ctrl + Left
            elif raw_c == b'\x1b[1;5D':
                c = "kLFT5"

            #            Ctrl + Left
            elif raw_c == b'\x1b[1;5D':
                c = "kLFT5"
            elif raw_c.startswith(b'\x1b[<0;'):
                strsplit = str(raw_c).split(';')
                x = strsplit[-2]
                y = strsplit[-1]
                click(x, y)
                c = ""
                raw_c = ""
            elif raw_c.startswith(b'\x1b[<2'):
                strsplit = str(raw_c).split(';')
                x = strsplit[-2]
                y = strsplit[-1]
                click(x, y, right=True)
                c = ""
                raw_c = ""
            #History page up/down
            elif raw_c.startswith(b'\x1b[5~'):
                screens[out_put_screen].prev_page()
                #debug(str(screens[out_put_screen].history.position))
                c = ""
                raw_c = ""
                #update_display(rerender = True)
            elif raw_c.startswith(b'\x1b[6~'):
                screens[out_put_screen].next_page()
                #debug(str(screens[out_put_screen].history.position))
                c = ""
                raw_c = ""
                #update_display(rerender = True)
            else:
                c = raw_c.decode("utf8")
                #debug("Unhandled KEY: "+ str(raw_c)) #TODO don't let this happen
        else:
            #the only esc key that is one char long is... esc... :)
            if c == "\x1b":
                c = "KEY_ESC"
        debug(f"KEYs setup: {c}")
        ############################################################################################################
        #####KEYs are setup, raw_c is input data, c is a char or a readable name like KEY_FISH###########
        ############################################################################################################
        composit_process((c, raw_c))


    #hanle ctrl+c
    except (KeyboardInterrupt, Exception) as E:
        if isinstance(E, KeyboardInterrupt):
            debug("ctrlc")
            c = 'CTRL_C'
            force_while_loop = True
        else:
            debug("Keyboard Error: " + str(E))
            raise(E)

        #mouse input (TODO, needs redone)
        """
        if '\x1b[<' == c:
        c = ""
        debug("Mouse Event")
        nextKeys = ""
        while True:
            if nextKeys.endswith("m") or (nextKeys.endswith("M") and nextKeys.startswith("64")):
            break
            nextKeys = nextKeys + sys.stdin.read(1)
        oldData = mouseEvent
        mouseEvent = nextKeys.split("\x1b[<")
        #double click to enter text
        if str(oldData).lower() == str(mouseEvent).lower() and nextKeys.startswith("0"):
            #check how fast
            timeDiff = time.time() - clickTime
            if timeDiff < .5:
            #curses.mousemask(curses.ALL_MOUSE_EVENTS)
            debug("MouseMode set to select")
            curses.mousemask(0)
            selectionMode = True
            else:
            debug("not a double click... too slow")
        else:
            debug(str(oldData) + " != " + str(mouseEvent))
        debug("EventInfo: " + str(mouseEvent))
        clickTime = time.time()
        else:
        mouseEvent = []
        """



