# setup
 - (Destkop only) sudo apt install libgpm-dev
 - (Destkop only) gcc ./gpm_reader.cpp -lgpm -lstdc++ -o gpm_reader
 - (Destkop only) sudo cp ./gpm_reader /usr/bin/
 - ctrl + alt + f1
 - run ./compositerm.py
 - run echo "45,0:90,44:/bin/bash" >> /dev/shm/compositerm/hook

#license

GPL3 By David Hamner
