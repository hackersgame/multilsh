#include <stdio.h>
#include <gpm.h>
#include <fstream>
//Thank you! https://www.linuxjournal.com/article/4600
std::ofstream outfile;
int my_handler(Gpm_Event *event, void *data)
{       
        outfile.open("/dev/shm/gpm_pipe", std::ios::out | std::ios::app);
        outfile << event->type << ":" << event->x << ":" << event->y << "\n";
        outfile.close();
        //printf("%d:%d:%d\n", event->type, event->x, event->y);
        return 0;
}
int main()
{       Gpm_Connect conn;
        int c;
        
        conn.eventMask  = ~0;   /* Want to know about all the events */
        conn.defaultMask = 0;   /* don't handle anything by default  */
        conn.minMod     = 0;    /* want everything                   */  
        conn.maxMod     = ~0;   /* all modifiers included            */
        
        
        if(Gpm_Open(&conn, 0) == -1)
                printf("Cannot connect to mouse server\n");
        
        printf("cat /dev/shm/gpm_pipe");
        gpm_handler = my_handler;
        while((c = Gpm_Getc(stdin)) != EOF)
                outfile << c;
        Gpm_Close();
        return 0;
}
