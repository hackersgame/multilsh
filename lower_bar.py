#!/usr/bin/python3
import datetime
import time

from uicon import *
toggle = 0
vt_cursor_off()
vt_clear()

#TODO pass in when started
compositerm_size = [90,90]
#      rows, cols
size = [2,90]
click_map = {}
start_app_text = "   run"
minimized = []

script_path = os.path.dirname(os.path.realpath(__file__))
run_menu = "run_menu.py"

def open_app(row, col, row_len, col_len, cmd):
    with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"{row},{col}:{col_len},{row_len}:{cmd}\n")

def close_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"close:{index}\n")

def min_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"min:{index}\n")
        
def max_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"max:{index}\n")

def get_running(simple=False):
    stuff_running = {}
    with open("/dev/shm/compositerm/running") as fh:
        for running_app in fh.readlines():
            index,cmd = running_app.split(":")
            if simple:
                if "/" in cmd:
                    simple_cmd = cmd.split("/")[-1].strip()
                else:
                    simple_cmd = cmd.strip()
                stuff_running[simple_cmd] = index
            else:
                stuff_running[index] = cmd
    return stuff_running

def process_click(row,col):
    global click_map
    global start_app_text
    global compositerm_size
    global minimized
    col_len = size[1]
    row_len = size[0]
    
    for button_name in click_map:
        button_data = click_map[button_name]
        if ":" in button_name:
            button_name, clicked_index = button_name.split(":")
        start_row, start_col = button_data[0]
        num_rows, num_cols =  button_data[1]
        if col >= start_col:
            if col < start_col + num_cols:
                #print(f"Withing col {button_name}")
                if  row >= start_row:
                    if row < start_row + num_rows:
                        #WE have a click :)
                        #run was clicked
                        if button_name == start_app_text:
                            run_cmd = script_path + "/" + run_menu
                            running_apps = get_running(simple=True)
                            if run_menu not in running_apps:

                                row_len = 20
                                col_len = 40
                                
                                row = compositerm_size[0] - row_len - 2
                                col = compositerm_size[1] - col_len - 1
                                open_app(row, col, row_len, col_len, run_cmd)
                                #print(f"Open {run_cmd}")
                            else:
                                index = running_apps[run_menu]
                                close_app(index)
                                #print(f"Close {index}")
                        #A program was clicked
                        else:
                            index = int(clicked_index.strip())
                            #not needed as this is passed in with the button name
                            
                            #running_apps = get_running(simple=True)
                            #try:
                            #    index = running_apps[button_name]
                            #except KeyError:
                            #    print(f"Error reading program index for {button_name}\n{click_map}")
                            #    continue
                            
                            #print(f"button: { button_name} at index: {index}\n{click_map}\n{get_running()}")
                            if index not in minimized:
                                minimized.append(index)
                                #print(f"min {index}")
                                min_app(index)
                                #send min 
                            else:
                                minimized.remove(index)
                                #print(f"max {index}")
                                max_app(index)
                                #send max
                        return()


#This also updates the click_map
def print_bot():
    global click_map
    global start_app_text
    row_vt = ""
    col_len = size[1]
    click_map = {}
    apps_running = get_running()
    num_things_to_write = len(apps_running) + 1
    size_each_gets = int(col_len)/int(num_things_to_write)
    for app_index in range(0, len(apps_running)):
        app_index = list(apps_running)[app_index]
        app = apps_running[app_index].strip()
        if "/" in app:
            app = app.split("/")[-1].strip()
        app = app + ":" + str(app_index)
        if len(app) >= size_each_gets:
            col = len(row_vt)
            row_vt = row_vt + app[:size_each_gets]
            #                     [[row,col],[num_rows,num_cols      ]]
            click_map[app] = [[0  ,col],[2       ,size_each_gets]]
        else:
            col = len(row_vt)
            clickmap_size = len(app)
            extra_space_len = int(size_each_gets - len(app))
            extra_spaces = " " * extra_space_len
            row_vt = row_vt + app + extra_spaces
            #                     [[row,col],[num_rows,num_cols     ]]
            click_map[app] = [[0  ,col],[2       ,clickmap_size]]
    space_left = " " * int(size_each_gets - len(start_app_text))
    
    clickmap_size = len(start_app_text)
    col = col_len - clickmap_size
    
    #Add in color
    row_vt = color_as_vt("black","blue",False) + row_vt + space_left
    row_vt = row_vt + color_as_vt("black","green",False) 
    row_vt = row_vt + start_app_text
    
    #                           [[row,col],[num_rows,num_cols     ]]
    click_map[start_app_text] = [[0  ,col],[2       ,clickmap_size]]
    #return(str(click_map))
    return(row_vt)
#clear does not work well with background colors within compositerm. This is a bad workaround. :) 
#def clear_by_dump():
#    vt_move(0,0)
#    x, y = vt_size()
#    spaces = " " * (x * y)
#    vt_write(spaces)

for event in uicon():
    if "init" in event[0]:
        vt_move(0,0)
        vt_write(print_bot())
    if "left_mouse_up" in event[0]:
        col,row = event[0].split(" ")[-2:]
        process_click(int(row),int(col))
        
        #redraw
        time.sleep(0.2)
        vt_move(0,0)
        vt_write(print_bot())
        #vt_move(y,x)
        #vt_write('o')
        #print(x,y)
        
    #print(f"DRIVER: {event}")
