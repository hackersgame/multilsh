#!/usr/bin/python3
import datetime
import time

from uicon import *
toggle = 0
vt_cursor_off()
vt_clear()

#TODO pass in when started
size = [2,90]
compositerm_size = [90,90]
script_path = os.path.dirname(os.path.realpath(__file__))
run_keyboard = "run_keyboard.py"

def open_app(row, col, row_len, col_len, cmd):
    with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"{row},{col}:{col_len},{row_len}:{cmd}\n")

def close_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"close:{index}")

def min_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"min:{index}")
        
def max_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"max:{index}")

def get_running(simple=False):
    stuff_running = {}
    with open("/dev/shm/compositerm/running") as fh:
        for running_app in fh.readlines():
            index,cmd = running_app.split(":")
            if simple:
                if "/" in cmd:
                    simple_cmd = cmd.split("/")[-1].strip()
                else:
                    simple_cmd = cmd
                stuff_running[simple_cmd] = index
            else:
                stuff_running[index] = cmd
    return stuff_running

def print_top():
    col_len = size[1]
    date_now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    white_space = int((col_len - len(date_now))/2)
    white_space = " " * white_space
    blank = " " * col_len
    return(white_space + date_now + white_space + "\n" + blank)
#clear does not work well with background colors within compositerm. This is a bad workaround. :) 
#def clear_by_dump():
#    vt_move(0,0)
#    x, y = vt_size()
#    spaces = " " * (x * y)
#    vt_write(spaces)

for event in uicon():
    if "left_mouse_up" in event[0] or "init" in event[0]:
        if toggle == 0:
            vt_move(0,0)
            vt_write(color_as_vt("black","blue",False))
            vt_write(print_top())
            toggle = 1
            
            #min keyboard
            run_cmd = script_path + "/" + run_keyboard
            running_apps = get_running(simple=True)
            if run_keyboard in running_apps:
                min_app(running_apps[run_keyboard])
        else:
            vt_move(0,0)
            vt_write(color_as_vt("black","green",False))
            vt_write(print_top())
            toggle = 0
            
            #open keyboard
            run_cmd = script_path + "/" + run_keyboard
            running_apps = get_running(simple=True)
            if run_keyboard not in running_apps:

                row_len = 40
                col_len = 90
                
                row = compositerm_size[0] - row_len - 2
                col = compositerm_size[1] - col_len
                open_app(row, col, row_len, col_len, run_cmd)
            else:
                max_app(running_apps[run_keyboard])
        #exclude init
        if event[0] == "left_mouse_up":
            x,y = event[0].split(" ")[-2:]
        #vt_move(y,x)
        #vt_write('o')
        #print(x,y)
        
    #print(f"DRIVER: {event}")
