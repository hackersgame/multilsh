#!/usr/bin/python3
import datetime
import time

from uicon import *
toggle = 0
vt_cursor_off()
vt_clear()

#TODO pass in when started
#      rows, cols
size = [20,40]
script_path = os.path.dirname(os.path.realpath(__file__))
run_menu = "run_menu.py"
#echo "2,0:90,40:/bin/bash" >> /dev/shm/compositerm/hook

def open_app(row, col, row_len, col_len, cmd):
    with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"{row},{col}:{col_len},{row_len}:{cmd}\n")
def close_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"close:{index}\n")

def min_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"min:{index}\n")
        
def max_app(index):
     with open("/dev/shm/compositerm/hook", 'a') as fh:
        fh.write(f"max:{index}\n")

def get_running(simple=False):
    stuff_running = {}
    with open("/dev/shm/compositerm/running") as fh:
        for running_app in fh.readlines():
            index,cmd = running_app.split(":")
            if simple:
                if "/" in cmd:
                    simple_cmd = cmd.split("/")[-1].strip()
                else:
                    simple_cmd = cmd
                stuff_running[simple_cmd] = index
            else:
                stuff_running[index] = cmd
    return stuff_running

def print_top():
    col_len = size[1]
    row_len = size[0]
    commands = "Run Bash"
    white_space = int((col_len - len(commands))/2)
    white_space = " " * white_space
    blank = ""
    for i in range(0,row_len-2):
        blank = blank + "." * col_len + "\n"
    return(white_space + commands + white_space + "\n" + blank)
#clear does not work well with background colors within compositerm. This is a bad workaround. :) 
#def clear_by_dump():
#    vt_move(0,0)
#    x, y = vt_size()
#    spaces = " " * (x * y)
#    vt_write(spaces)

wait_for_exit = False
for event in uicon():
    if "init" in event[0]:
        vt_move(0,0)
        vt_write(color_as_vt("black","blue",False))
        vt_write(print_top())
    
    elif "left_mouse_up" in event[0]:
        if wait_for_exit:
            print("END")
            continue
        x,y = event[0].split(" ")[-2:]
        #catch close and echo on this tty
        if int(y) < 0 or int(x) < 0:
            print("Bye")
            continue
        vt_move(0,0)
        #vt_write(color_as_vt("black","blue",False))
        #vt_write(print_top())
        #toggle = 1
        
        #set close menu
        running_apps = get_running(simple=True)
        if run_menu in running_apps:
            index = running_apps[run_menu]
            close_app(index)
        else:
            print(f"{run_menu} not in {running_apps}")
        
        #open Bash
        row_len = 40
        col_len = 90
        open_app(2, 0, row_len, col_len, "bash")
        
        time.sleep(0.3)

        wait_for_exit = True
        #vt_move(y,x)
        #vt_write('o')
        #print(x,y)
        
    #print(f"DRIVER: {event}")
